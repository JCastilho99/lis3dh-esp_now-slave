//Includes
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "config.h"

extern "C"
{
#include <espnow.h>
}

//Defines
#define WIFI_CHANNEL 6
#define SEND_INTERVAL 12000
#define MAXREADINGS 6

//Global variables
uint8_t macMaster[] = {0xCC, 0x50, 0xE3, 0xC7, 0x67, 0x80};															//MAC address do esp8266 "slave"
uint8_t macSlave[] = {0x3C, 0x71, 0xBF, 0x2A, 0xD9, 0x23};															//Endereço MAC do controlador
uint8_t key[16] = {0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44}; //chave para a comunicação esp_now
uint8_t kok[16] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66}; //chave para a chave
unsigned long previousMillis = 0;
unsigned long previousMillisIO = 0;
volatile boolean haveReading = false;
int count = 0;

struct LIS3DH_DATA
{ //struct com os dados do sensor
	float x;
	float y;
	float z;
} rtcMem;

LIS3DH_DATA dataArray[MAXREADINGS + 1];

//Configuração da rede
const char *MY_SSID = "The Loop co.";
const char *MY_PWD = "theloop5019#";
const char *MY_SSID2 = "iPhone de João Pedro";
const char *MY_PWD2 = "hckn9jeu4iigp";

//AdafruitIO feeds
AdafruitIO_Feed *x_g = io.feed("acc.acc-x");
AdafruitIO_Feed *y_g = io.feed("acc.acc-y");
AdafruitIO_Feed *z_g = io.feed("acc.acc-z");

//Functions
void getReading(uint8_t *data, uint8_t len);
void sendToAdafruitIO(LIS3DH_DATA array[]);
void ConnectToIO();
void initWiFi();
void initEspNow();

void setup()
{
	Serial.begin(115200);
	Serial.println();

	//o ESP_NOW tem de ser inicializado no setup porque ele vai estar sempre a aguardar mensagens de esp_now
	initEspNow();
}

void loop()
{
	//Mensagem de debug
	unsigned long currentMillis = millis();

	if (currentMillis - previousMillis > 10000)
	{
		Serial.println("Waiting for ESP_NOW messages...");
		previousMillis = currentMillis;
	}

	//Quando são efetuadas 6 leituras
	if (count == MAXREADINGS)
	{
		//Desligamos o esp_now para podermos ligar o wifi
		esp_now_deinit();
		initWiFi();

		//Ligamos e enviamos ao Adafruit
		ConnectToIO();
		sendToAdafruitIO(dataArray);
		io.run();

		//Voltamos a desligar o WiFi e damos reset ao contador
		WiFi.disconnect();
		count = 0;

		//Restart do ESP
		ESP.restart();
	}
}

void getReading(uint8_t *data, uint8_t len)
{
	//prints received data to serial
	LIS3DH_DATA tmp;
	memcpy(&tmp, data, len);

	Serial.print(", data, ");
	Serial.print('\t');
	Serial.print(tmp.x);
	Serial.print('\t');
	Serial.print(tmp.y);
	Serial.print('\t');
	Serial.println(tmp.z);
}

void initWiFi()
{
	WiFi.mode(WIFI_AP_STA);
	WiFi.begin(MY_SSID, MY_PWD);

	Serial.print("Connecting to WiFi");
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}

	Serial.printf("WiFi channel: %d\n", wifi_get_channel());
	Serial.println("WiFi Connected!!");
	Serial.print("IP : ");
	Serial.println(WiFi.localIP());

	Serial.print("This node AP mac; ");
	Serial.print(WiFi.softAPmacAddress());
	Serial.print(", STA mac: ");
	Serial.println(WiFi.macAddress());
}

void initEspNow()
{
	if (esp_now_init() != 0)
	{
		Serial.println("*** ESP_Now init failed ***");
		ESP.restart();
	}
	delay(1); //vi na net que este delay ajudava https://github.com/HarringayMakerSpace/IoT/blob/master/ESP-Now/espnow-sensorNode/espnow-sensorNode.ino
	esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);

	//Configuração da encryptação
	esp_now_set_kok(kok, 16);
	esp_now_add_peer(macMaster, ESP_NOW_ROLE_CONTROLLER, WIFI_CHANNEL, key, 16);
	esp_now_set_peer_key(macMaster, key, 16);
	Serial.println("ESP_NOW success!");

	esp_now_register_recv_cb([](uint8_t *mac, uint8_t *data, uint8_t len) { //gets called every time it receives data
		memcpy(&rtcMem, data, sizeof(rtcMem));
		char macString[50] = {0};
		sprintf(macString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		Serial.print(macString);
		getReading(data, len);

		//Guarda a leitura no array e incrementa o count
		dataArray[count] = rtcMem;
		count++;
	});
}

void ConnectToIO()
{
	Serial.print("Connecting to AdafruitIO");
	io.connect();
	while (io.status() < AIO_CONNECTED)
	{
		Serial.print(".");
		delay(500);
	}
	Serial.println();
	Serial.println(io.statusText());
}

void sendToAdafruitIO(LIS3DH_DATA array[])
{
	for (int i = 0; i < MAXREADINGS; i++)
	{
		unsigned long currentMillis = millis();
		if (currentMillis - previousMillisIO > 200)
		{
			x_g->save(dataArray[i].x);
			y_g->save(dataArray[i].y);
			z_g->save(dataArray[i].z);
			Serial.printf("Reading nº %d sent!\n", i);
			previousMillisIO = currentMillis;
		}
	}
}